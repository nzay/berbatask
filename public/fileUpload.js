$(document).ready(function(){
    $('#fileUploadAlert').hide();
    $('#link').hide();
    $('#loading').hide();
    $('#fileStatus').hide();

    
    //holds the mateCat Project details
    mateCatProjectDetails = {};
   
    //Define EVent Listener
    var files = [];
    document.getElementById("files").addEventListener("change", function(e) {
    files = e.target.files;
    });

    
    //File Upload Code
    document.getElementById("send").addEventListener("click", function() {
        //checks if files are selected
        if (files.length != 0) {

            //create a storage reference
            var storage = firebase.storage().ref(files[0].name);

            //upload file
            var upload = storage.put(files[0]);

            //update progress bar
            upload.on(
                "state_changed",
                function progress(snapshot) {
                    var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    document.getElementById("progress").value = percentage;
                },

                function error() {
                    alert("error uploading file");
                },

                function complete() {
                    //document.getElementById("uploading").innerHTML += `${files[0].name} upoaded to storage <br />`;
                    $('#fileUploadAlert').show();
                    var storage = firebase.storage().ref(files[0].name);
                    //get file url from Firebase
                    storage.getDownloadURL().then(function(url) {
                        console.log(url);
                        $('#fileStatus').show();
                        sendPostToAPI();
                    })
                    .catch(function(error) {
                        console.log(error)
                        console.log("error encountered");
                    });
                }
            );
            
            } else {
            alert("No file chosen");
            }
    });


    /**
     * Sends a POST Request to matecat to create a new Project
     * 
     */
    function sendPostToAPI() {
        $('#loading').show();
        var form = new FormData();
        form.append("files", files[0], files[0].name);
        form.append("project_name", "testing122");
        form.append("source_lang", "en-US");
        form.append("tms_engin", "1");
        form.append("mt_engine", "1");
        form.append("subject", "general");
        form.append("owner_email", "nuzairnuwais@gmail.com");
        form.append("target_lang", "es-ES");
        form.append("get_public_matches", "true");

        var settings = {
        "url": "https://www.matecat.com/api/v1/new",
        "method": "POST",
        "timeout": 0,
        "headers": {
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form
        };

        $.ajax(settings).done(function (response) {
        
            mateCatProjectDetails = JSON.parse(response);
            //$('#step1').html(JSON.stringify(mateCatProjectDetails, undefined, 4));
            console.log(mateCatProjectDetails)
            $('#id_project').attr("placeholder", mateCatProjectDetails.id_project);
            $('#project_pass').attr("placeholder", mateCatProjectDetails.project_pass);
            
            setTimeout(() => {  getAnalysisInfo(); }, 2000);
            
            
        });
    }

    /**
     * Send a GET request to get analysis info
     */
    function getAnalysisInfo(){
        var analuyzeURl = "https://www.matecat.com/api/status?id_project="+mateCatProjectDetails.id_project+"&project_pass="+mateCatProjectDetails.project_pass;
        console.log(analuyzeURl);
        var settings = {
            "url":analuyzeURl,
            "method": "GET",
            "timeout": 0,
            "headers": {
            },
          };
          
          $.ajax(settings).done(function (response) {
            console.log(response);
            // var analysis = JSON.parse(response);
            $('#link').attr("href", response.analyze);
            $('#link').show();
            //$('#step2').html(JSON.stringify(response, undefined, 4));
            $('#TOTAL_RAW_WC').attr("placeholder", response.data.summary.TOTAL_RAW_WC);
            $('#loading').hide();
          });
    }

});
